package kv.in.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ListQuizes extends AppCompatActivity implements View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {
    DataBaseUtility dataBaseUtility;
    Button q1, q2, q3, q4, q5;
    Intent intent;
    TextView nav,navEmail;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView=navigationView.getHeaderView(0);
        nav=(TextView)headerView.findViewById(R.id.nav_text);
        navEmail=(TextView)headerView.findViewById(R.id.nav_email);
        dataBaseUtility=new DataBaseUtility(ListQuizes.this);
        String email=getIntent().getStringExtra("email");
        navEmail.setText(email);
        nav.setText(dataBaseUtility.getName(email));
        intent = new Intent(ListQuizes.this, QuizActivity.class);
        q1 = (Button) findViewById(R.id.quiz_1);
        q2 = (Button) findViewById(R.id.quiz_2);
        q3 = (Button) findViewById(R.id.quiz_3);
        q4 = (Button) findViewById(R.id.quiz_4);
        q5 = (Button) findViewById(R.id.quiz_5);
        q1.setOnClickListener(this);
        q2.setOnClickListener(this);
        q3.setOnClickListener(this);
        q4.setOnClickListener(this);
        q5.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.quiz_1:
                intent.putExtra("qname", "quiz_1");
                intent.putExtra("qno",1);
                startActivity(intent);
                break;
            case R.id.quiz_2:
                intent.putExtra("qname", "quiz_2");
                intent.putExtra("qno",1);
                startActivity(intent);
                break;
            case R.id.quiz_3:
                intent.putExtra("qname", "quiz_3");
                intent.putExtra("qno",1);
                startActivity(intent);
                break;
            case R.id.quiz_4:
                intent.putExtra("qname", "quiz_4");
                intent.putExtra("qno",1);
                startActivity(intent);
                break;
            case R.id.quiz_5:
                intent.putExtra("qname", "quiz_5");
                intent.putExtra("qno",1);
                startActivity(intent);
                break;

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
}
