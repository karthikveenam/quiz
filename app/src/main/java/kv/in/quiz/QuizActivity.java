package kv.in.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {
    RadioButton o1,o2,o3,o4;
    TextView question;
    DataBaseUtility dataBaseUtility;
    Button next;
    String quizName;

    int questionNo;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_layout);
    dataBaseUtility=new DataBaseUtility(QuizActivity.this);

    question=(TextView)findViewById(R.id.question);
    o1=(RadioButton)findViewById(R.id.o1);
        o2=(RadioButton)findViewById(R.id.o2);
        o3=(RadioButton)findViewById(R.id.o3);
        o4=(RadioButton)findViewById(R.id.o4);
        next=(Button)findViewById(R.id.next);
        quizName=getIntent().getStringExtra("qname");
        questionNo=getIntent().getIntExtra("qno",1);
        if(questionNo==5)
            next.setText("Submit");
        if(questionNo==6)
        {
            Intent intent=new Intent(QuizActivity.this,ResultsActivity.class);
            intent.putExtra("qname",quizName);
            startActivity(intent);
        }
        dataBaseUtility.createTable(quizName);
        if(dataBaseUtility.getCount(quizName)!=5)
        {
            addDataToQuiz(quizName);
        }

      //  Toast.makeText(QuizActivity.this,quizName,Toast.LENGTH_LONG).show();
//Toast.makeText(QuizActivity.this,Integer.toString(questionNo),Toast.LENGTH_LONG).show();
//dataBaseUtility.addQuestion(quizName,1,"How are you?","Fine","Not Fine","Both","None","Fine");
        question.setText(dataBaseUtility.getQuestion(quizName,questionNo));
    o1.setText(dataBaseUtility.getOption(quizName,questionNo,1));
        o2.setText(dataBaseUtility.getOption(quizName,questionNo,2));
        o3.setText(dataBaseUtility.getOption(quizName,questionNo,3));
        o4.setText(dataBaseUtility.getOption(quizName,questionNo,4));

    next.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
  dataBaseUtility.saveOption(quizName,questionNo,getCheckedOption());
  //Toast.makeText(QuizActivity.this,dataBaseUtility.getSavedOption(quizName,questionNo),Toast.LENGTH_LONG).show();
  //Toast.makeText(QuizActivity.this,dataBaseUtility.getCorrectOption(quizName,questionNo),Toast.LENGTH_LONG).show();
            Intent intent=new Intent(QuizActivity.this,QuizActivity.class);
            intent.putExtra("qname",quizName);
            intent.putExtra("qno",questionNo+1);
            startActivity(intent);
        }
    });
    }
   public  String getCheckedOption()
    {
      if(o1.isChecked())
          return o1.getText().toString();
      else if(o2.isChecked())
          return o2.getText().toString();
      else if( o3.isChecked())
          return  o3.getText().toString();
      else
          return o4.getText().toString();
    }
    void addDataToQuiz(String quiz)
    {

        dataBaseUtility.addQuestion(quiz,1,quiz+" question  no 1",quiz+" op 1 ",quiz+" op 2",quiz+" op 3",quiz+" op 4",quiz+" op 2");
        dataBaseUtility.addQuestion(quiz,2,quiz+" question  no 2",quiz+" op 1 ",quiz+" op 2",quiz+" op 3",quiz+" op 4",quiz+" op 4");
        dataBaseUtility.addQuestion(quiz,3,quiz+" question  no 3",quiz+" op 1 ",quiz+" op 2",quiz+" op 3",quiz+" op 4",quiz+" op 3");
        dataBaseUtility.addQuestion(quiz,4,quiz+" question  no 4",quiz+" op 1 ",quiz+" op 2",quiz+" op 3",quiz+" op 4",quiz+" op 1");
        dataBaseUtility.addQuestion(quiz,5,quiz+" question  no 5",quiz+" op 1 ",quiz+" op 2",quiz+" op 3",quiz+" op 4",quiz+" op 2");
    }

}
