package kv.in.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity{
    EditText email,password;
    Button login;
    TextView reg;
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        email=(EditText)findViewById(R.id.email_id);
        password=(EditText)findViewById(R.id.password);
        login=(Button)findViewById(R.id.login);
        reg=(TextView)findViewById(R.id.register);
login.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view)
    {

                DataBaseUtility dataBaseUtility=new DataBaseUtility(LoginActivity.this);
                boolean isValid= dataBaseUtility.authenticateUser(email.getText().toString(),password.getText().toString());
                if(isValid) {
                    Intent intent=new Intent(LoginActivity.this,ListQuizes.class);
                    intent.putExtra("email",email.getText().toString());

                    startActivity(intent);
                }
                    else
                    Toast.makeText(LoginActivity.this,"Invalid Credentials",Toast.LENGTH_LONG).show();

    }
});
    reg.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
        }
    });
    }
}
