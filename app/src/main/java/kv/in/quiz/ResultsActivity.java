package kv.in.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ResultsActivity extends AppCompatActivity{
    TextView score;
    DataBaseUtility dataBaseUtility;
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.results_layout);
    score=(TextView)findViewById(R.id.score);
    dataBaseUtility=new DataBaseUtility(ResultsActivity.this);
score.setText("Your total score is "+Integer.toString(dataBaseUtility.getScore(getIntent().getStringExtra("qname"))));
    }
}
