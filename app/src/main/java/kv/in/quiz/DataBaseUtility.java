package kv.in.quiz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.strictmode.SqliteObjectLeakedViolation;

import java.nio.file.attribute.UserDefinedFileAttributeView;

public class DataBaseUtility extends SQLiteOpenHelper {
private static final String DATABASE_NAME="QuizDB.db";
private static final int DATABASE_VERSION=1;
    private static final String ID_COLUMN="id";
private static final String NAME_COLUMN="name";
private static final String EMAIL_COLUMN="email";
    private static final String PASSWORD_COLUMN="password";
    private static final String NEW_TABLE_COMMAND="CREATE TABLE Registrations("+ID_COLUMN+" INTEGER PRIMARY KEY AUTOINCREMENT,"+NAME_COLUMN+" TEXT,"+EMAIL_COLUMN+" TEXT,"+PASSWORD_COLUMN+" TEXT)";
    private static final String DROP_TABLE_COMMAND="DROP TABLE IF EXISTS Registrations";
    public DataBaseUtility(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
sqLiteDatabase.execSQL(NEW_TABLE_COMMAND);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
sqLiteDatabase.execSQL(DROP_TABLE_COMMAND);
onCreate(sqLiteDatabase);
    }
    public void addUser(UserData user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NAME_COLUMN, user.getName());
        values.put(EMAIL_COLUMN, user.getEmail());
        values.put(PASSWORD_COLUMN, user.getPassword());

        // Inserting Row
        db.insert("Registrations", null, values);
        db.close();
    }
    public boolean isUserExist(String email)
    {
        String[] resultCols={ID_COLUMN};
        String[] selectCols={email};
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor = db.query("Registrations", //Table to query
                resultCols,                    //columns to return
                EMAIL_COLUMN+" = ?",                  //columns for the WHERE clause
                selectCols,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null);
        int count=cursor.getCount();
        cursor.close();
        db.close();
        if(count>0)
            return true;
        return false;
    }

    public boolean authenticateUser(String email,String password)
    {
        String[] resultCols={ID_COLUMN};
        String[] selectCols={email,password};
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor = db.query("Registrations", //Table to query
                resultCols,                    //columns to return
                EMAIL_COLUMN+" = ? AND "+PASSWORD_COLUMN+" = ?",                  //columns for the WHERE clause
                selectCols,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null);
        int count=cursor.getCount();
        cursor.close();
        db.close();
        if(count==0)
            return false;
        return true;
    }

    public String getName(String email)
    {

        String qn="";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor =db.rawQuery("SELECT * FROM Registrations WHERE "+EMAIL_COLUMN+"= '"+email+"'",null);
        if(cursor.moveToNext())
            qn=cursor.getString(1);
        cursor.close();
        return qn;

    }
    public void addQuestion(String tableName,int qNo,String Question,String opt1,String opt2,String opt3,String opt4,String correctAnswer)
    {
SQLiteDatabase db;

db=this.getWritableDatabase();
ContentValues values=new ContentValues();
values.put("q_no",qNo);
values.put("Question",Question);
values.put("opt1",opt1);
values.put("opt2",opt2);
values.put("opt3",opt3);
values.put("opt4",opt4);
values.put("correct_answer",correctAnswer);
db.insert(tableName,null,values);
db.close();
    }
public void createTable(String tableName)
{
    String CREATE_QUIZ_TABLE="CREATE TABLE IF NOT EXISTS "+tableName+"(q_no INTEGER PRIMARY KEY AUTOINCREMENT,Question TEXT,opt1 TEXT,opt2 TEXT,opt3 TEXT,opt4 TEXT,correct_answer TEXT,option_recorded TEXT,is_correct INTEGER) ";
SQLiteDatabase sqLiteDatabase=getWritableDatabase();
sqLiteDatabase.execSQL(CREATE_QUIZ_TABLE);
}
    public String getQuestion(String tableName,int qNo)
    {

        String qn="";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor =db.rawQuery("SELECT * FROM "+tableName+" WHERE q_no="+qNo,null);
        if(cursor.moveToNext())
  qn=cursor.getString(1);
  cursor.close();
     return qn;

    }
    public String getOption(String tableName,int qNo,int oNo)
    {

        String qn="";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor =db.rawQuery("SELECT * FROM "+tableName+" WHERE q_no="+qNo,null);
        if(cursor.moveToNext())
            qn=cursor.getString(1+oNo);
        cursor.close();
        return qn;

    }
    public int getCount(String tableName)
    {
int count;
        String qn="";
        SQLiteDatabase db=this.getReadableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT * FROM " + tableName, null);
            count=cursor.getCount();
            cursor.close();

        }
        catch (SQLiteException s)
        {
            count=0;
        }
        return count;
    }
    public void saveOption(String qName,int qNo,String option)
    {
        String updateQuery="UPDATE "+qName+" SET option_recorded = "+"'"+option+"'"+" WHERE q_no="+qNo;
        SQLiteDatabase sqLiteDatabase;
        sqLiteDatabase=getWritableDatabase();
        sqLiteDatabase.execSQL(updateQuery);
        sqLiteDatabase.close();
    }

    public int getScore(String qName)
    {
        String qn="";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor =db.rawQuery("SELECT * FROM "+qName+" Q1 WHERE Q1.correct_answer=Q1.option_recorded",null);
int n=cursor.getCount();
        cursor.close();
        return n;
    }
}
