package kv.in.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity{
EditText name;
EditText email;
EditText password;
Button registerButton;
TextView nav_name;
private DataBaseUtility dataBaseUtility;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);

        dataBaseUtility=new DataBaseUtility(MainActivity.this);
name=(EditText)findViewById(R.id.name);
email=(EditText)findViewById(R.id.email_id);
password=(EditText)findViewById(R.id.password);
registerButton=(Button) findViewById(R.id.register);
registerButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
                //Toast.makeText(MainActivity.this,"done",Toast.LENGTH_LONG).show();
                UserData userData=new UserData();
                userData.setEmail(email.getText().toString());
                userData.setName(name.getText().toString());
                userData.setPassword(password.getText().toString());
if(isEmailValid(email.getText().toString())) {
    //   DataBaseUtility dataBaseUtility=new DataBaseUtility(MainActivity.this);
    if (!dataBaseUtility.isUserExist(email.getText().toString()))
        dataBaseUtility.addUser(userData);
    else
        Toast.makeText(MainActivity.this, "Account already exists, Please login", Toast.LENGTH_LONG).show();
    Intent activityIntent = new Intent(MainActivity.this, LoginActivity.class);
    startActivity(activityIntent);
}
else
    Toast.makeText(MainActivity.this,"Enter valid email",Toast.LENGTH_LONG).show();
}});
    }

public boolean isEmailValid(String email)
{
    if(Patterns.EMAIL_ADDRESS.matcher(email).matches())
        return true;
    return false;
}

}
